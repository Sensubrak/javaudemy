package com.sensubrak;

public class BankAccount {
    private String number;
    private double balance;
    private String customerName;
    private String email;
    private String phoneNumber;

    public BankAccount() {
        this("56789", 2.5, "Default name",
                "Default address", "Default phone");
        System.out.println("Empty constructor called");
    }

    public BankAccount(String number, double balance, String customerName, String email, String phoneNumber) {
        System.out.println("Account constructor with parameters called");
        this.number = number;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public BankAccount(String customerName, String email, String phoneNumber) {
        this("99999", 100.55, customerName, email, phoneNumber);
    }

    public void depositFunds(double funds) {
        this.balance += funds;
        System.out.println("Deposit of " + funds + " made. New balance is " + this.balance);
    }

    public void withdrawFunds(double funds) {
        if(this.balance >= funds) {
            this.balance -= funds;
            System.out.println("Withdrawal of " + funds + " processed. Remaining balance = " + this.balance);
        }
        else {
            System.out.println("Only " + this.balance + " available. Withdrawal not processed");
        }
    }

    public void setNumber(String accountNumber) {
        this.number = accountNumber;
    }
    public String getNumber() {
        return this.number;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
    public double getBalance() {
        return this.balance;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public String getCustomerName() {
        return this.customerName;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return this.email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getPhoneNumber() {
        return this.phoneNumber;
    }
}
