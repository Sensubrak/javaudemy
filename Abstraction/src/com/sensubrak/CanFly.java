package com.sensubrak;

public interface CanFly {
    void fly();
}
